package com.mcy.springboottest.repository;

import com.mcy.springboottest.entity.Book;
import com.mcy.springboottest.entity.Role;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class RoleRepositoryTest {

    @Autowired
    private RoleRepository roleRepository;

    @Test
    void findAll() {
        System.out.println(roleRepository.findAll());
    }

    @Test
    void save() {
        Role role = new Role();
        role.setRolename("test");
        Role role1 = roleRepository.save(role);
        System.out.println(role1);
    }
}