package com.mcy.springboottest.repository;

import com.mcy.springboottest.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class UserRepositoryTest {
    @Autowired
    private UserRepository userRepository;

    @Test
    public void login() {
        List<User> users = userRepository.findAll();
        for (User user: users) {
            if (user.getUsername().equals("admin") && user.getPassword().equals("admin")) System.out.println("success");
        }
        System.out.println("error");
    }

}