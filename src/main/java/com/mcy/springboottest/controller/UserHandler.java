package com.mcy.springboottest.controller;

import com.mcy.springboottest.entity.User;
import com.mcy.springboottest.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserHandler {
    @Autowired
    private UserRepository userRepository;

    @GetMapping("/findAll")
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @GetMapping("/findById/{id}")
    public User findById(@PathVariable("id") Integer id) {
        return userRepository.findById(id).get();
    }

    @PostMapping("/save")
    public String save(@RequestBody User user) {
        User result = userRepository.save(user);
        if (result != null) {
            return "success";
        }else {
            return "error";
        }
    }

    @PutMapping("/update")
    public String update(@RequestBody User user) {
        User result = userRepository.save(user);
        if (result != null) {
            return "success";
        }else {
            return "error";
        }
    }

    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") Integer id) {
        userRepository.deleteById(id);
    }

    @RequestMapping("/login/{username}/{password}")
    public String login(@PathVariable("username") String username, @PathVariable("password") String password) {
        List<User> users = userRepository.findAll();
        for (User user: users) {
            //叫你用==比较字符串
            if (user.getUsername().equals(username) && user.getPassword().equals(password)) return "success";
        }
        return "error";
    }

}
