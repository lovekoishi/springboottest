package com.mcy.springboottest.controller;

import com.mcy.springboottest.entity.Role;
import com.mcy.springboottest.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/role")
public class RoleHandler {
    @Autowired
    private RoleRepository roleRepository;

    @GetMapping("/findAll")
    public List<Role> findAll() {
        return roleRepository.findAll();
    }

    @GetMapping("/findById/{id}")
    public Role findById(@PathVariable("id") Integer id) {
        return roleRepository.findById(id).get();
    }

    @PostMapping("/save")
    public String save(@RequestBody Role role) {
        Role result = roleRepository.save(role);
        if (result != null) {
            return "success";
        }else {
            return "error";
        }
    }

    @PutMapping("/update")
    public String update(@RequestBody Role role) {
        Role result = roleRepository.save(role);
        if (result != null) {
            return "success";
        }else {
            return "error";
        }
    }

    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") Integer id) {
        roleRepository.deleteById(id);
    }
}
