package com.mcy.springboottest.repository;

import com.mcy.springboottest.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Integer> {
}
